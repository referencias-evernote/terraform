terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configura o provider da AWS
provider "aws" {
  access_key = "AKIAWXJSJ7NROPUMESC7"
  secret_key = "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"
  region = "sa-east-1"
}

resource "aws_instance" "dev-lab-2" {
    ami = "ami-054a31f1b3bf90920"
    instance_type = "t2.micro"
    key_name = "aws-lab"
    subnet_id = "subnet-b2d747d4"
    tags = {
      Name = "dev-lab-2"
    }
    vpc_security_group_ids = ["${aws_security_group.acesso-SSH.id}"]
    depends_on = [
      aws_dynamodb_table.dynamodb-hml
    ]
}

resource "aws_instance" "desenvolvimento" {
    count = "3"
    ami = "ami-054a31f1b3bf90920"
    instance_type = "t2.micro"
    key_name = "aws-lab"
    subnet_id = "subnet-b2d747d4"
    tags = {
      Name = "hml${count.index}"
    }
    vpc_security_group_ids = ["${aws_security_group.acesso-SSH.id}"]
}

resource "aws_instance" "dev-lab" {
    ami = "ami-054a31f1b3bf90920"
    instance_type = "t2.micro"
    key_name = "aws-lab"
    subnet_id = "subnet-b2d747d4"
    tags = {
      Name = "dev-lab"
    }
    vpc_security_group_ids = ["${aws_security_group.acesso-SSH.id}"]
    depends_on = [
      aws_s3_bucket.dev4
    ]
}

resource "aws_dynamodb_table" "dynamodb-hml" {
  name           = "GameScores"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "UserId"
  range_key      = "GameTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "GameTitle"
    type = "S"
  }
}