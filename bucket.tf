resource "aws_s3_bucket" "dev4" {
  bucket = "bucket-dev-corneliuz"
  acl    = "private"

  tags = {
    Name = "bucket-dev-corneliuz"
  }
}