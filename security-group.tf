resource "aws_security_group" "acesso-SSH" {
  name        = "acesso-ssh"
  description = "Acesso SSh"

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["189.54.220.143/32"]
  }
  tags = {
    Name = "ssh"
  }
}